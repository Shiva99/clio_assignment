FROM node:12
WORKDIR /usr/src/app
COPY package.json /usr/src/app/package.json
RUN npm install
COPY . /usr/src/app
CMD ["node", "server.js"]
EXPOSE 8080
